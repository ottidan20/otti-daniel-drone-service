<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->

[![Java][Java]][Java-url]
[![Spring-boot][Spring-boot]][Spring-boot-url]
[![PostgreSql-db][PostgreSql-db]][PostgreSql-db-url]
[![Docker][Docker]][Docker-url]


<!-- PROJECT LOGO -->
<br />

[//]: # (<div align="center">)

[//]: # (  <a href="https://github.com/othneildrew/Best-README-Template">)

[//]: # (<img src="images/logo.png" alt="Logo" width="80" height="80">)

[//]: # (  </a>)


<h3 align="center">Drone Service Api Assessment Solution</h3>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#testing-endpoints">Testing Endpoints</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

This API provide endpoints for:
* Drones management
* Medications management
* Dispatching Medications via Drones.

The project is an assessment solution for a drone service.

All API input/output data are in JSON format

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

The project is developed using the following technologiea.

* [![Java][Java]][Java-url]
* [![Spring-boot][Spring-boot]][Spring-boot-url]
* [![PostgreSql-db][PostgreSql-db]][PostgreSql-db-url]
* [![Docker][Docker]][Docker-url]


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites
* Java JDK 17
* Docker
* Maven
* Supported IDE of your choice.

### Installation
To set up the project, please follow the following instructions:

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ottidan20/otti-daniel-drone-service.git 
   ```

2. **Launch PostgreSQL Container** <br />Open Terminal or CLI, navigate to the project directory and run PostgreSQL database container via docker compose. The configuration are in the docker-compose.yaml file. You can also do this with the help of your IDE plugins support.
   ```sh
   docker compose up -d
   ```
   Note: Without this step, project wont run, as it would look for an active database to seed initial data.
3. **Install Project dependencies with Maven**
   Open project directory in your IDE and make sure you install all maven dependencies.

4. **Run Project** <br />
   After successful initialization and installation of dependencies, click on the run button in your ide to build and start the project.
   Alternatively, open the project directory in a terminal and enter the following command:
   ```sh
   mvn spring-boot:run
   ```
   On successful run operation, the project uri is accessible on localhost via <a>http://localhost:8081/api/v1
<p align="right">(<a href="#readme-top">back to top</a>)</p>

5. **JUNIT Test cases** <br />
   You could also run the JUnit test cases to assert that    everything is working correctly. I wrote test cases to assert every functional requirement of the service.

<!-- USAGE EXAMPLES -->
## Testing Endpoints
All HTTP requests can be done via any HTTPClient like Postman or ThunderClient. Post request are implemented by sending JSON formatted data to the endpoints to be validated and processed.

**Some of the assumption made for the purpose of this API design are:-**

- Once a Medication item is loaded to a specific drone it cannot be loaded to another drone at the same time.
- Once the process of loading a drone commences, its state is updated to LOADING, if it succeeds it changes to Loaded lese it reverts bck to IDLE.

### Registering A Drone
#### API Endpoint: http://localhost:8081/api/v1/drone/registerDrone
>To register a drone, a request formatted as in the example below is sent. Data sent is validated against required constraints and drone is registered when constraints are passed. The API returns a JSON response of all registered drones.<br />
**Method: POST**

**Payload**
```sh
{
  "serialNumber": "Q23RT5676698",
  "model": "MIDDLEWEIGHT",
  "weightLimit": 200,
  "batteryCapacity": 0.54,
  "state": "IDLE"
}

```

**Response:**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:27:28.510081327",
  "serialNumber": "Q23RT5676698",
  "message": "Drone registered successfully"
}

```

### Checking available drones for loading
#### API Endpoint: http://localhost:8081/api/v1/drone/getAvailableDrones
>Endpoint retrieves a collection of drones available for loading.<br />
**Method: GET**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:18:09.737156444",
  "drones": [
    {
      "id": 1,
      "createdDate": "2022-12-03T07:13:26.075+00:00",
      "updatedDate": "2022-12-03T07:13:26.075+00:00",
      "version": 0,
      "serialNumber": "13dfa1b2-e0d7-4072-a75d-2eb26da041d8",
      "model": "CRUISEWEIGHT",
      "weightLimit": 346.82,
      "batteryCapacity": 0.44,
      "state": "IDLE"
    },
    {
      "id": 2,
      "createdDate": "2022-12-03T07:13:26.266+00:00",
      "updatedDate": "2022-12-03T07:13:26.266+00:00",
      "version": 0,
      "serialNumber": "e95b5fd8-269f-4425-bba7-70e15e9bfe36",
      "model": "CRUISEWEIGHT",
      "weightLimit": 127.27,
      "batteryCapacity": 0.84,
      "state": "IDLE"
    },
  ]
}
  ```

### Getting all Medication items
#### API Endpoint: http://localhost:8081/api/v1/medication/getAllMedications
>Endpoint retrieves a collection of medications<br />
**Method: GET**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:28:38.119452921",
  "medications": [
    {
      "id": 1,
      "createdDate": "2022-12-03T07:13:27.394+00:00",
      "updatedDate": "2022-12-03T07:13:27.394+00:00",
      "version": 0,
      "name": "Amoxicillin",
      "weight": 336.7,
      "code": "4a48eaee-df6e-4936-9bea-c28c2a75b3ea",
      "imageUri": "4a48eaee-df6e-4936-9bea-c28c2a75b3ea"
    },
    {
      "id": 2,
      "createdDate": "2022-12-03T07:13:27.445+00:00",
      "updatedDate": "2022-12-03T07:13:27.445+00:00",
      "version": 0,
      "name": "Abarelix",
      "weight": 274.54,
      "code": "cc054621-eab1-419a-bf15-c8c4a2d0fac2",
      "imageUri": "cc054621-eab1-419a-bf15-c8c4a2d0fac2"
    },
  ]
}
  ```
  Note: the Medication items to be loaded for testing have alredy been taken care of. I implemented data seeders with Faker plugin, to load inital set of medications and drones to database on successful startup of project.

### Loading a drone with medication items
#### API Endpoint: http://localhost:8081/api/v1/drone/loadDroneWithMedication
>Endpoint retrieves a collection of drones available for loading.<br />
**Method: POST**<br />

The payload will have the following fields
- serialNumber is the unique serial for the drone being loaded
- code the unique code for the medication load being loaded to the drone
- source is the loading point
- destination is where the load is being taken
- the destination and the source are any places
- Also if the weight limit is exceeded while trying to load a drone, you would be prompted to use another medication/drone.

**Payload**:

```sh
{
  "serialNumber": "7a990b54-be8c-4a88-8300-d86246d50f8b",
  "source": "Lagos",
  "destination": "Abuja",
  "code": "aed20777-6a36-4798-8db8-f9a22cdf263a"
}

```
**Response**:

```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:30:15.040788946",
  "serialNumber": "Q23RT5676698",
  "message": "Drone loaded with medication successfully"
}

```
### Checking Loaded Medication Items For A Given Drone
#### API Endpoint: http://localhost:8081/api/v1/drone/getLoadedDroneDetails/{serialNumber}
>Endpoint retrieves medication load details for a loaded drone.<br />
**Method: GET**<br />

**Response**
```sh 
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:35:26.614380916",
  "serialNumber": "Q23RT5676698",
  "medication": {
    "id": 8,
    "createdDate": "2022-12-03T07:13:27.635+00:00",
    "updatedDate": "2022-12-03T07:13:27.635+00:00",
    "version": 0,
    "name": "Covax",
    "weight": 192.08,
    "code": "750a0e2d-7380-4c17-a137-32c914121730",
    "imageUri": "750a0e2d-7380-4c17-a137-32c914121730"
  }
}
```

### Checking Battery Level For A Given Drone
#### API Endpoint: http://localhost:8081/api/v1/drone/getDroneBatteryLevel/{serialNumber}
>Endpoint retrieves battery level details for a drone.<br />
**Method: GET**<br />

**Response**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:38:53.926535617",
  "serialNumber": "Q23RT5676698",
  "batteryCapacity": "54%"
}
```

### Delivering a medication item to destination via drone
#### API Endpoint: http://localhost:8081/api/v1/drone/deliverMedication
>Endpoint retrieves battery level details for a drone.<br />
**Method: POST**<br />

**Payload**
```sh
{
  "serialNumber": "7a990b54-be8c-4a88-8300-d86246d50f8b"
}
```

**Response**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T11:45:57.309098087",
  "serialNumber": "Q23RT5676698",
  "message": "Medication load delivered successfully"
}
```

### Retrieve battery event log
#### API Endpoint: http://localhost:8081/api/v1/drone/getBatteryLogs
* Request Type: GET
>GET request to retrieve battery level log of scheduled task checking drone battery levels.
<br />For the purpose of this test, 2 scheduled tasks were implemented:
> * A scheduler that logs the current battery level of all valid drones. it does periocically every 60 seconds.
> * The scheduler also saves the audit logs to the database.

**Response**
```sh
{
  "result": "Success",
  "timeStamp": "2022-12-03T12:08:50.062738014",
  "batteryLogs": [
    {
      "id": 1,
      "createdDate": "2022-12-03T11:08:45.530+00:00",
      "updatedDate": "2022-12-03T11:08:45.530+00:00",
      "version": 0,
      "droneSerialNumber": "e077d1b5-11de-4cfa-b613-e050bda47b4e",
      "batteryCapacity": 0.89
    },
    {
      "id": 2,
      "createdDate": "2022-12-03T11:08:45.572+00:00",
      "updatedDate": "2022-12-03T11:08:45.572+00:00",
      "version": 0,
      "droneSerialNumber": "bc50c761-51d4-48d0-867f-0a42c14a020d",
      "batteryCapacity": 0.94
    },
    {
      "id": 3,
      "createdDate": "2022-12-03T11:08:45.586+00:00",
      "updatedDate": "2022-12-03T11:08:45.586+00:00",
      "version": 0,
      "droneSerialNumber": "a26bbf6e-4e16-452b-8cb9-6d048a039de1",
      "batteryCapacity": 0.87
    },
  ]
}
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/uthman-a-bakar/
[product-screenshot]: images/screenshot.png


[Spring-boot]:https://img.shields.io/badge/Spring_Boot-F2F4F9?style=for-the-badge&logo=spring-boot
[Spring-boot-url]: https://spring.io/projects/spring-boot
[Java]:https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white
[Java-url]: https://java.com
[PostgreSql-db]:https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSql-db-url]: https://www.postgresql.org/
[Docker]:https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://docker.com
