package com.example.otti_daniel_drone_service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.example.otti_daniel_drone_service.controller.DroneController;
import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.model.EModel;
import com.example.otti_daniel_drone_service.model.EState;
import com.example.otti_daniel_drone_service.model.Medication;
import com.example.otti_daniel_drone_service.payload.request.LoadDroneWithMedicationRequest;
import com.example.otti_daniel_drone_service.payload.request.RegisterDroneRequest;
import com.example.otti_daniel_drone_service.payload.response.GetAvailableDronesResponse;
import com.example.otti_daniel_drone_service.payload.response.GetDroneBatteryLevelResponse;
import com.example.otti_daniel_drone_service.payload.response.GetLoadedDroneDetailsResponse;
import com.example.otti_daniel_drone_service.payload.response.LoadDroneWithMedicationResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterDroneResponse;
import com.example.otti_daniel_drone_service.repository.DroneRepository;
import com.example.otti_daniel_drone_service.service.DroneService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class TestDroneServices {
    @InjectMocks
    DroneController droneController;

    @Mock
    DroneService droneService;

    @Mock
    DroneRepository droneRepository;

    /**
     * Test that register drone works
     */
    @Test
    public void testRegisterDrone() {
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockHttpServletRequest));

        RegisterDroneResponse registerDroneResponse = new RegisterDroneResponse();
        when(droneService.registerDrone(any(RegisterDroneRequest.class))).thenReturn(registerDroneResponse);

        RegisterDroneRequest registerDroneRequest = new RegisterDroneRequest("WEUIDF79FDDF", EModel.LIGHTWEIGHT, 490,
                new BigDecimal(0.98),
                EState.IDLE);
        ResponseEntity<RegisterDroneResponse> responseEntity = droneController.registerDrone(registerDroneRequest);
        Assertions.assertThat(responseEntity.getStatusCode().toString()).isEqualTo("201 CREATED");
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(registerDroneResponse);

        droneRepository.deleteAll();

    }

    @Test
    public void testGetAvailableDronesForLoading() {
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockHttpServletRequest));

        List<Drone> drones = new ArrayList<Drone>();
        Drone drone1 = new Drone("WED23R8SADA676697", EModel.CRUISEWEIGHT, 500.0, new BigDecimal(0.95), EState.IDLE);
        Drone drone2 = new Drone("WED23R8SADA676698", EModel.HEAVYWEIGHT, 150.0, new BigDecimal(1), EState.IDLE);
        Drone drone3 = new Drone("WED23R8SADA676699", EModel.LIGHTWEIGHT, 200.0, new BigDecimal(0.98), EState.IDLE);
        drones.add(drone1);
        drones.add(drone2);
        drones.add(drone3);

        GetAvailableDronesResponse getAvailableDronesResponse = new GetAvailableDronesResponse();
        getAvailableDronesResponse.setDrones(drones);

        when(droneService.getAvailableDrones(EState.IDLE)).thenReturn(getAvailableDronesResponse);

        ResponseEntity<GetAvailableDronesResponse> responseEntity = droneController.getAvailableDrones();

        Assertions.assertThat(responseEntity.getStatusCode().toString()).isEqualTo("200 OK");
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(getAvailableDronesResponse);
    }

    @Test
    public void testCheckDroneBatteryLevel() {
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockHttpServletRequest));

        GetDroneBatteryLevelResponse getDroneBatteryLevelResponse = new GetDroneBatteryLevelResponse("WED23R8SADA67669",
                "98%");

        when(droneService.getBatteryLevel(any(String.class))).thenReturn(getDroneBatteryLevelResponse);

        ResponseEntity<GetDroneBatteryLevelResponse> responseEntity = droneController
                .getDroneBatteryLevel("WED23R8SADA676699");
        Assertions.assertThat(responseEntity.getStatusCode().toString()).isEqualTo("200 OK");
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(getDroneBatteryLevelResponse);
    }

    @Test
    public void testDroneWithMedicationLoad() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        LoadDroneWithMedicationResponse loadDroneWithMedicationResponse = new LoadDroneWithMedicationResponse();
        loadDroneWithMedicationResponse.setResult("Success");
        loadDroneWithMedicationResponse.setMessage("Drone loaded with medication successfully");
        loadDroneWithMedicationResponse.setTimeStamp(java.time.LocalDateTime.now());
        loadDroneWithMedicationResponse.setSerialNumber("WED23R8SADA676699");

        when(droneService.loadDroneWithMedication(any(LoadDroneWithMedicationRequest.class)))
                .thenReturn(loadDroneWithMedicationResponse);

        LoadDroneWithMedicationRequest loadDroneWithMedicationRequest = new LoadDroneWithMedicationRequest();
        loadDroneWithMedicationRequest.setCode("WED23R8SADA676699");
        loadDroneWithMedicationRequest.setDestination("Abuja");
        loadDroneWithMedicationRequest.setSource("Lagos");
        loadDroneWithMedicationRequest.setCode("GRE1223465G");

        ResponseEntity<LoadDroneWithMedicationResponse> responseEntity = droneController
                .loadDroneWithMedication(loadDroneWithMedicationRequest);
        Assertions.assertThat(responseEntity.getStatusCode().toString()).isEqualTo("201 CREATED");
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(loadDroneWithMedicationResponse);

    }

    @Test
    public void testCheckLoadedMedicationItem() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Medication medication = new Medication();
        medication.setCode("ISDONDJ238932");
        medication.setName("Paracetamol");
        medication.setWeight(350);
        medication.setImageUri(null);

        GetLoadedDroneDetailsResponse getLoadedDroneDetailsResponse = new GetLoadedDroneDetailsResponse();
        getLoadedDroneDetailsResponse.setMedication(medication);
        getLoadedDroneDetailsResponse.setResult("Success");
        getLoadedDroneDetailsResponse.setSerialNumber("EW7328SDNKD");
        getLoadedDroneDetailsResponse.setTimeStamp(java.time.LocalDateTime.now());

        String serialNumber = "Q23RT5676697";
        when(droneService.checkLoadedMedicationForDrone(serialNumber)).thenReturn(getLoadedDroneDetailsResponse);

        ResponseEntity<GetLoadedDroneDetailsResponse> responseEntity = droneController
                .getLoadedDroneDetails(serialNumber);
        Assertions.assertThat(responseEntity.getStatusCode().toString()).isEqualTo("200 OK");
        Assertions.assertThat(responseEntity.getBody()).isEqualTo(getLoadedDroneDetailsResponse);

    }
}
