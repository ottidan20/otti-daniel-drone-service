package com.example.otti_daniel_drone_service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.otti_daniel_drone_service.controller.DroneController;
import com.example.otti_daniel_drone_service.controller.MedicationController;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class OttiDanielDroneServiceApplicationTests {

	@Autowired
	DroneController droneController;

	@Autowired
	MedicationController medicationController;

	/**
	 * Test that controllers are visible in class path context
	 */
	@Test
	void contextLoads() {
		Assertions.assertThat(droneController).isNotNull();
		Assertions.assertThat(medicationController).isNotNull();
	}

}
