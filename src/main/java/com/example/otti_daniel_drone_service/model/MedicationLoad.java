package com.example.otti_daniel_drone_service.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "medication_load")
@Data
@EqualsAndHashCode(callSuper = false)
public class MedicationLoad extends BaseEntity {

    @NotBlank
    private String source;

    @NotBlank
    private String destination;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_drone_id", referencedColumnName = "id")
    private Drone drone;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_medication_id", referencedColumnName = "id")
    private Medication medication;
}
