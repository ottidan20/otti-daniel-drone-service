package com.example.otti_daniel_drone_service.model;

public enum EModel {
    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISEWEIGHT, HEAVYWEIGHT
}
