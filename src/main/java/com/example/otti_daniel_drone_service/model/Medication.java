package com.example.otti_daniel_drone_service.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "medication")
@Data
@EqualsAndHashCode(callSuper = false)
public class Medication extends BaseEntity {

    @NotBlank
    @Column(unique = true)
    private String name;

    @NotNull
    private double weight;

    @NotBlank
    @Column(unique = true)
    private String code;

    @Column(nullable = true)
    private String imageUri;
}
