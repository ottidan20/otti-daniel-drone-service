package com.example.otti_daniel_drone_service.model;

public enum EState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
