package com.example.otti_daniel_drone_service.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "medication_delivery")
@Data
@EqualsAndHashCode(callSuper = false)
public class MedicationDelivery extends BaseEntity {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_medication_load_id", referencedColumnName = "id")
    private MedicationLoad medicationLoad;
}
