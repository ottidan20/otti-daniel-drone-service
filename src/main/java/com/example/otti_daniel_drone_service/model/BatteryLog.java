package com.example.otti_daniel_drone_service.model;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "battery_log")
@Data
@EqualsAndHashCode(callSuper = false)
public class BatteryLog extends BaseEntity {
    @NotBlank
    private String droneSerialNumber;

    @NotNull
    private BigDecimal batteryCapacity;
}
