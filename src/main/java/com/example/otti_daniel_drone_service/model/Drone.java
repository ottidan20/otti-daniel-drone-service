package com.example.otti_daniel_drone_service.model;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "drone")
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Drone extends BaseEntity {

    @NotBlank
    @Column(unique = true)
    @Size(max = 100)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private EModel model;

    @NotNull
    @Max(500)
    private double weightLimit;

    @NotNull
    private BigDecimal batteryCapacity;

    @NotNull
    @Enumerated(EnumType.STRING)
    private EState state;
}
