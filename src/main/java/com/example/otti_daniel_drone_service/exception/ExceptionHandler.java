package com.example.otti_daniel_drone_service.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.client.HttpClientErrorException.Unauthorized;

import com.example.otti_daniel_drone_service.payload.response.CustomMessageResponse;

import jakarta.persistence.EntityNotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(value = { RuntimeException.class })
    public ResponseEntity<CustomMessageResponse> handleInvalidInput(RuntimeException runtimeException) {
        logger.error("Invalid input", runtimeException.getMessage());

        CustomMessageResponse customMessageResponse = new CustomMessageResponse();
        customMessageResponse.setMessage(runtimeException.getMessage());
        customMessageResponse.setResult("Failed");
        customMessageResponse.setTimeStamp(java.time.LocalDateTime.now());

        return new ResponseEntity<CustomMessageResponse>(customMessageResponse, HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = { Unauthorized.class })
    public ResponseEntity<CustomMessageResponse> handleUnauthorized(RuntimeException runtimeException) {
        logger.error("Unauthorized", runtimeException.getMessage());

        CustomMessageResponse customMessageResponse = new CustomMessageResponse();
        customMessageResponse.setMessage(runtimeException.getMessage());
        customMessageResponse.setResult("Failed");
        customMessageResponse.setTimeStamp(java.time.LocalDateTime.now());

        return new ResponseEntity<CustomMessageResponse>(customMessageResponse, HttpStatus.UNAUTHORIZED);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = { Exception.class })
    public ResponseEntity<CustomMessageResponse> handleException(RuntimeException runtimeException) {
        logger.error("Unexpected error", runtimeException.getMessage());

        CustomMessageResponse customMessageResponse = new CustomMessageResponse();
        customMessageResponse.setMessage(runtimeException.getMessage());
        customMessageResponse.setResult("Failed");
        customMessageResponse.setTimeStamp(java.time.LocalDateTime.now());

        return new ResponseEntity<CustomMessageResponse>(customMessageResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = { EntityNotFoundException.class })
    public ResponseEntity<CustomMessageResponse> handleEntityNotFound(RuntimeException runtimeException) {
        logger.error("Entity not found", runtimeException.getMessage());

        CustomMessageResponse customMessageResponse = new CustomMessageResponse();
        customMessageResponse.setMessage(runtimeException.getMessage());
        customMessageResponse.setResult("Failed");
        customMessageResponse.setTimeStamp(java.time.LocalDateTime.now());

        return new ResponseEntity<CustomMessageResponse>(customMessageResponse, HttpStatus.NOT_FOUND);
    }
}
