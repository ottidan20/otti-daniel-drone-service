package com.example.otti_daniel_drone_service;

import java.math.BigDecimal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.example.otti_daniel_drone_service.model.EModel;
import com.example.otti_daniel_drone_service.model.EState;
import com.example.otti_daniel_drone_service.payload.request.RegisterDroneRequest;
import com.example.otti_daniel_drone_service.payload.request.RegisterMedicationRequest;
import com.example.otti_daniel_drone_service.repository.DroneRepository;
import com.example.otti_daniel_drone_service.repository.MedicationRepository;
import com.example.otti_daniel_drone_service.service.DroneServiceImpl;
import com.example.otti_daniel_drone_service.service.MedicationServiceImpl;
import com.github.javafaker.Faker;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@SpringBootApplication
@EnableJpaAuditing
@OpenAPIDefinition
public class OttiDanielDroneServiceApplication {

	@Autowired
	private DroneRepository droneRepository;

	@Autowired
	private MedicationRepository medicationRepository;

	@Autowired
	private DroneServiceImpl droneServiceImpl;

	@Autowired
	private MedicationServiceImpl medicationServiceImpl;

	public static void main(String[] args) {
		SpringApplication.run(OttiDanielDroneServiceApplication.class, args);
	}

	/**
	 * Seeds initial data for drones and medications
	 * 
	 * @return success
	 */
	@Bean
	CommandLineRunner commandLineRunner() {
		return args -> {
			Faker faker = new Faker(new Locale("en-US"));

			// Check if Drone repository is empty
			if (droneRepository.count() < 1) {
				RegisterDroneRequest registerDroneRequest = new RegisterDroneRequest();

				// Seed drones
				for (int i = 0; i < 10; i++) {
					registerDroneRequest.setSerialNumber(faker.internet().uuid());
					registerDroneRequest.setBatteryCapacity(new BigDecimal(faker.number().randomDouble(2, 0, 1)));
					registerDroneRequest.setModel(EModel.CRUISEWEIGHT);
					registerDroneRequest.setState(EState.IDLE);
					registerDroneRequest.setWeightLimit(faker.number().randomDouble(2, 20, 500));
					droneServiceImpl.registerDrone(registerDroneRequest);
				}

				for (int i = 0; i < 10; i++) {
					registerDroneRequest.setSerialNumber(faker.internet().uuid());
					registerDroneRequest.setBatteryCapacity(new BigDecimal(faker.number().randomDouble(2, 0, 1)));
					registerDroneRequest.setModel(EModel.HEAVYWEIGHT);
					registerDroneRequest.setState(EState.IDLE);
					registerDroneRequest.setWeightLimit(faker.number().randomDouble(2, 20, 500));
					droneServiceImpl.registerDrone(registerDroneRequest);
				}

				for (int i = 0; i < 10; i++) {
					registerDroneRequest.setSerialNumber(faker.internet().uuid());
					registerDroneRequest.setBatteryCapacity(new BigDecimal(faker.number().randomDouble(2, 0, 1)));
					registerDroneRequest.setModel(EModel.LIGHTWEIGHT);
					registerDroneRequest.setState(EState.IDLE);
					registerDroneRequest.setWeightLimit(faker.number().randomDouble(2, 20, 500));
					droneServiceImpl.registerDrone(registerDroneRequest);
				}

				for (int i = 0; i < 10; i++) {
					registerDroneRequest.setSerialNumber(faker.internet().uuid());
					registerDroneRequest.setBatteryCapacity(new BigDecimal(faker.number().randomDouble(2, 0, 1)));
					registerDroneRequest.setModel(EModel.MIDDLEWEIGHT);
					registerDroneRequest.setState(EState.IDLE);
					registerDroneRequest.setWeightLimit(faker.number().randomDouble(2, 20, 500));
					droneServiceImpl.registerDrone(registerDroneRequest);
				}
			}

			// Seed Medications
			if (medicationRepository.count() < 1) {
				RegisterMedicationRequest registerMedicationRequest = new RegisterMedicationRequest();
				String medications[] = new String[] { "Amoxicillin", "Abarelix", "Haicon",
						"Haldol", "Talacen",
						"Ativan", "Diprolene", "Covax", "Rabavert", "Danocrine" };

				for (int i = 0; i < 10; i++) {
					registerMedicationRequest.setCode(faker.internet().uuid());
					registerMedicationRequest.setImageUri(faker.internet().image());
					registerMedicationRequest.setWeight(faker.number().randomDouble(2, 1, 500));
					registerMedicationRequest.setName(medications[i]);
					medicationServiceImpl.registerMedication(registerMedicationRequest);
				}
			}

		};
	}

}
