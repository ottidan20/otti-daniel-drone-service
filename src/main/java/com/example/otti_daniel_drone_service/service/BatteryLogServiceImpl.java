package com.example.otti_daniel_drone_service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.otti_daniel_drone_service.model.BatteryLog;
import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.payload.response.GetBatteryLogsResponse;
import com.example.otti_daniel_drone_service.repository.BatteryLogRepository;
import com.example.otti_daniel_drone_service.repository.DroneRepository;

@Service
public class BatteryLogServiceImpl implements BatteryLogService {

    @Autowired
    private BatteryLogRepository batteryLogRepository;

    @Autowired
    private DroneRepository droneRepository;

    @Override
    public void logBatteryLevels() {
        List<Drone> drones = droneRepository.findAll();
        BatteryLog batteryLog = new BatteryLog();

        for (Drone drone : drones) {
            batteryLog.setDroneSerialNumber(drone.getSerialNumber());
            batteryLog.setBatteryCapacity(drone.getBatteryCapacity());
            batteryLog.setVersion(drone.getVersion());
            batteryLogRepository.save(batteryLog);
        }
    }

    @Override
    public GetBatteryLogsResponse getBatteryLogs() {
        GetBatteryLogsResponse getBatteryLogsResponse = new GetBatteryLogsResponse();
        List<BatteryLog> batteryLogs = batteryLogRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        getBatteryLogsResponse.setBatteryLogs(batteryLogs);
        getBatteryLogsResponse.setResult("Success");
        getBatteryLogsResponse.setTimeStamp(java.time.LocalDateTime.now());
        return getBatteryLogsResponse;
    }

}
