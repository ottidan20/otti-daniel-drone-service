package com.example.otti_daniel_drone_service.service;

import org.springframework.stereotype.Component;

import com.example.otti_daniel_drone_service.payload.request.RegisterMedicationRequest;
import com.example.otti_daniel_drone_service.payload.response.GetAllMedicationsResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterMedicationResponse;

@Component
public interface MedicationService {
    RegisterMedicationResponse registerMedication(RegisterMedicationRequest registerMedicationRequest);

    GetAllMedicationsResponse getAllMedications();
}
