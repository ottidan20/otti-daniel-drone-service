package com.example.otti_daniel_drone_service.service;

import org.springframework.stereotype.Component;

import com.example.otti_daniel_drone_service.payload.response.GetBatteryLogsResponse;

@Component
public interface BatteryLogService {
    public void logBatteryLevels();

    GetBatteryLogsResponse getBatteryLogs();
}
