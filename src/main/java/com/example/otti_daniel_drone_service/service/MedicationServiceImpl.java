package com.example.otti_daniel_drone_service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.otti_daniel_drone_service.model.Medication;
import com.example.otti_daniel_drone_service.payload.request.RegisterMedicationRequest;
import com.example.otti_daniel_drone_service.payload.response.GetAllMedicationsResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterMedicationResponse;
import com.example.otti_daniel_drone_service.repository.MedicationRepository;

@Service
public class MedicationServiceImpl implements MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;

    @Override
    public RegisterMedicationResponse registerMedication(RegisterMedicationRequest registerMedicationRequest) {
        Medication medication = new Medication();
        medication.setCode(registerMedicationRequest.getCode());
        medication.setImageUri(registerMedicationRequest.getCode());
        medication.setWeight(registerMedicationRequest.getWeight());
        medication.setName(registerMedicationRequest.getName());

        if (medicationRepository.findbyName(registerMedicationRequest.getName()) != null)
            throw new RuntimeException("Name already in use!");

        if (medicationRepository.findByCode(registerMedicationRequest.getCode()) != null)
            throw new RuntimeException("Code already in use!");

        medicationRepository.save(medication);

        RegisterMedicationResponse registerMedicationResponse = new RegisterMedicationResponse();
        registerMedicationResponse.setCode(medication.getCode());
        registerMedicationResponse.setMessage("Medication registered successfully");
        registerMedicationResponse.setName(medication.getName());
        registerMedicationResponse.setResult("Success");
        registerMedicationResponse.setTimeStamp(java.time.LocalDateTime.now());

        return registerMedicationResponse;
    }

    @Override
    public GetAllMedicationsResponse getAllMedications() {
        GetAllMedicationsResponse getAllMedicationsResponse = new GetAllMedicationsResponse();
        List<Medication> medications = medicationRepository.findAll();
        getAllMedicationsResponse.setMedications(medications);
        getAllMedicationsResponse.setResult("Success");
        getAllMedicationsResponse.setTimeStamp(java.time.LocalDateTime.now());

        return getAllMedicationsResponse;
    }

}
