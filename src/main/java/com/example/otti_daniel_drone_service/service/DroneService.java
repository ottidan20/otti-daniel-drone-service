package com.example.otti_daniel_drone_service.service;

import org.springframework.stereotype.Component;

import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.model.EState;
import com.example.otti_daniel_drone_service.payload.request.DeliverDroneRequest;
import com.example.otti_daniel_drone_service.payload.request.LoadDroneWithMedicationRequest;
import com.example.otti_daniel_drone_service.payload.request.RegisterDroneRequest;
import com.example.otti_daniel_drone_service.payload.response.DeliverDroneResponse;
import com.example.otti_daniel_drone_service.payload.response.GetAvailableDronesResponse;
import com.example.otti_daniel_drone_service.payload.response.GetDroneBatteryLevelResponse;
import com.example.otti_daniel_drone_service.payload.response.GetLoadedDroneDetailsResponse;
import com.example.otti_daniel_drone_service.payload.response.LoadDroneWithMedicationResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterDroneResponse;

@Component
public interface DroneService {
    RegisterDroneResponse registerDrone(RegisterDroneRequest registerDroneRequest);

    GetAvailableDronesResponse getAvailableDrones(EState state);

    Drone getDrone(String serialNumber);

    GetDroneBatteryLevelResponse getBatteryLevel(String serialNumber);

    LoadDroneWithMedicationResponse loadDroneWithMedication(
            LoadDroneWithMedicationRequest loadDroneWithMedicationRequest);

    GetLoadedDroneDetailsResponse checkLoadedMedicationForDrone(String serialNumber);

    DeliverDroneResponse deliverMedicalLoad(DeliverDroneRequest deliverDroneRequest);

}
