package com.example.otti_daniel_drone_service.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.model.EState;
import com.example.otti_daniel_drone_service.model.Medication;
import com.example.otti_daniel_drone_service.model.MedicationDelivery;
import com.example.otti_daniel_drone_service.model.MedicationLoad;
import com.example.otti_daniel_drone_service.payload.request.DeliverDroneRequest;
import com.example.otti_daniel_drone_service.payload.request.LoadDroneWithMedicationRequest;
import com.example.otti_daniel_drone_service.payload.request.RegisterDroneRequest;
import com.example.otti_daniel_drone_service.payload.response.DeliverDroneResponse;
import com.example.otti_daniel_drone_service.payload.response.GetAvailableDronesResponse;
import com.example.otti_daniel_drone_service.payload.response.GetDroneBatteryLevelResponse;
import com.example.otti_daniel_drone_service.payload.response.GetLoadedDroneDetailsResponse;
import com.example.otti_daniel_drone_service.payload.response.LoadDroneWithMedicationResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterDroneResponse;
import com.example.otti_daniel_drone_service.repository.DroneDeliveryRepository;
import com.example.otti_daniel_drone_service.repository.DroneRepository;
import com.example.otti_daniel_drone_service.repository.MedicationLoadRepository;
import com.example.otti_daniel_drone_service.repository.MedicationRepository;

@Service
public class DroneServiceImpl implements DroneService {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationLoadRepository medicationLoadRepository;

    @Autowired
    private DroneDeliveryRepository droneDeliveryRepository;

    @Override
    public RegisterDroneResponse registerDrone(RegisterDroneRequest registerDroneRequest) {
        Drone drone = new Drone();
        drone.setSerialNumber(registerDroneRequest.getSerialNumber());
        drone.setModel(registerDroneRequest.getModel());
        drone.setWeightLimit(registerDroneRequest.getWeightLimit());
        drone.setBatteryCapacity(registerDroneRequest.getBatteryCapacity());
        drone.setState(registerDroneRequest.getState());

        if (droneRepository.findBySerialNumber(registerDroneRequest.getSerialNumber()) != null)
            throw new RuntimeException("Serial number already in use!");

        droneRepository.save(drone);

        RegisterDroneResponse registerDroneResponse = new RegisterDroneResponse();
        registerDroneResponse.setResult("Success");
        registerDroneResponse.setMessage("Drone registered successfully");
        registerDroneResponse.setSerialNumber(drone.getSerialNumber());
        registerDroneResponse.setTimeStamp(java.time.LocalDateTime.now());

        return registerDroneResponse;
    }

    @Override
    public GetAvailableDronesResponse getAvailableDrones(EState state) {
        GetAvailableDronesResponse getAvailableDronesResponse = new GetAvailableDronesResponse();
        List<Drone> drones = droneRepository.findAllByState(state);
        getAvailableDronesResponse.setDrones(drones);
        getAvailableDronesResponse.setResult("Success");
        getAvailableDronesResponse.setTimeStamp(java.time.LocalDateTime.now());

        return getAvailableDronesResponse;
    }

    @Override
    public Drone getDrone(String serialNumber) {
        Drone drone = droneRepository.findBySerialNumber(serialNumber);

        if (drone == null)
            throw new RuntimeException("Drone not found!");

        return drone;
    }

    @Override
    public GetDroneBatteryLevelResponse getBatteryLevel(String serialNumber) {
        Drone drone = new Drone();
        DecimalFormat decimalFormat = new DecimalFormat("#%");

        GetDroneBatteryLevelResponse getDroneBatteryLevelResponse = new GetDroneBatteryLevelResponse();
        drone.setSerialNumber(serialNumber);

        Drone droneBatteryLevel = droneRepository.findBySerialNumber(serialNumber);
        if (droneBatteryLevel == null)
            throw new RuntimeException("Battery level for drone not found!");

        getDroneBatteryLevelResponse.setResult("Success");
        getDroneBatteryLevelResponse.setBatteryCapacity(decimalFormat.format(droneBatteryLevel.getBatteryCapacity()));
        getDroneBatteryLevelResponse.setSerialNumber(serialNumber);
        getDroneBatteryLevelResponse.setTimeStamp(java.time.LocalDateTime.now());

        return getDroneBatteryLevelResponse;
    }

    @Override
    public LoadDroneWithMedicationResponse loadDroneWithMedication(
            LoadDroneWithMedicationRequest loadDroneWithMedicationRequest) {

        Drone drone = droneRepository.findBySerialNumber(loadDroneWithMedicationRequest.getSerialNumber());
        Medication medication = medicationRepository.findByCode(loadDroneWithMedicationRequest.getCode());
        MedicationLoad medicationLoad = medicationLoadRepository.findByCode(loadDroneWithMedicationRequest.getCode());

        // Checking if medication has already been loaded.
        if (medicationLoad != null)
            throw new RuntimeException("This Medication has already been loaded on a drone, try another one!");

        // Extra validations before loading
        if (drone == null)
            throw new RuntimeException("Drone does not exist!");
        if (medication == null)
            throw new RuntimeException("Medication does not exist!");
        if (drone.getWeightLimit() < medication.getWeight())
            throw new RuntimeException("Load requested is more than drone's weight limit");
        if (drone.getBatteryCapacity().compareTo(new BigDecimal(0.25)) < 0)
            throw new RuntimeException("This drone cannot be loaded because battery level is below 25%");

        // Change drone status to loading
        droneRepository.updateState(EState.LOADING, loadDroneWithMedicationRequest.getSerialNumber());

        // Now we can load the drone with validated information
        MedicationLoad medicationLoad2 = new MedicationLoad();
        medicationLoad2.setDrone(drone);
        medicationLoad2.setMedication(medication);
        medicationLoad2.setSource(loadDroneWithMedicationRequest.getSource());
        medicationLoad2.setDestination(loadDroneWithMedicationRequest.getDestination());
        medicationLoadRepository.save(medicationLoad2);

        LoadDroneWithMedicationResponse loadDroneWithMedicationResponse = new LoadDroneWithMedicationResponse();
        loadDroneWithMedicationResponse.setResult("Success");
        loadDroneWithMedicationResponse.setSerialNumber(loadDroneWithMedicationRequest.getSerialNumber());
        loadDroneWithMedicationResponse.setMessage("Drone loaded with medication successfully");
        loadDroneWithMedicationResponse.setTimeStamp(java.time.LocalDateTime.now());

        // Change drone status to loaded
        droneRepository.updateState(EState.LOADED, loadDroneWithMedicationRequest.getSerialNumber());

        return loadDroneWithMedicationResponse;
    }

    @Override
    public GetLoadedDroneDetailsResponse checkLoadedMedicationForDrone(String serialNumber) {
        MedicationLoad medicationLoad = medicationLoadRepository.findByDrone(serialNumber);

        if (medicationLoad == null)
            throw new RuntimeException("No medication load details found for this drone!");

        GetLoadedDroneDetailsResponse getLoadedDroneDetailsResponse = new GetLoadedDroneDetailsResponse();
        getLoadedDroneDetailsResponse.setMedication(medicationLoad.getMedication());
        getLoadedDroneDetailsResponse.setResult("Success");
        getLoadedDroneDetailsResponse.setSerialNumber(serialNumber);
        getLoadedDroneDetailsResponse.setTimeStamp(java.time.LocalDateTime.now());

        return getLoadedDroneDetailsResponse;
    }

    @Override
    public DeliverDroneResponse deliverMedicalLoad(DeliverDroneRequest deliverDroneRequest) {
        // Update state to delivering
        droneRepository.updateState(EState.DELIVERING, deliverDroneRequest.getSerialNumber());

        MedicationLoad medicationLoad = medicationLoadRepository.findByDrone(deliverDroneRequest.getSerialNumber());

        if (medicationLoad == null)
            throw new RuntimeException("Drone has not been loaded or does not exist");

        MedicationDelivery medicationDelivery = new MedicationDelivery();
        medicationDelivery.setMedicationLoad(medicationLoad);

        // Saving delivery details and update drone status to delivered
        droneDeliveryRepository.save(medicationDelivery);
        droneRepository.updateState(EState.DELIVERED, deliverDroneRequest.getSerialNumber());

        DeliverDroneResponse deliverDroneResponse = new DeliverDroneResponse();
        deliverDroneResponse.setMessage("Medication load delivered successfully");
        deliverDroneResponse.setResult("Success");
        deliverDroneResponse.setTimeStamp(java.time.LocalDateTime.now());
        deliverDroneResponse.setSerialNumber(deliverDroneRequest.getSerialNumber());

        return deliverDroneResponse;
    }

}
