package com.example.otti_daniel_drone_service.cron_jobs;

import java.text.DecimalFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.repository.DroneRepository;
import com.example.otti_daniel_drone_service.service.BatteryLogService;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@Component
public class MonitorDroneBatteryLevel {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private BatteryLogService batteryLogService;

    static final Logger logger = LoggerFactory.getLogger(MonitorDroneBatteryLevel.class);

    @Scheduled(fixedRate = 60000)
    public void monitorBatteryLevelTask() throws InterruptedException {
        logger.info("Getting and logging battery levels of all drones");
        List<Drone> drones = droneRepository.findAll();

        DecimalFormat decimalFormat = new DecimalFormat("#%");

        for (int i = 0; i < drones.size(); i++) {
            logger.info("Battery level of " + drones.get(i).getSerialNumber() + " is "
                    + decimalFormat.format(drones.get(i).getBatteryCapacity()));
        }
        Thread.sleep(5000);
    }

    @Scheduled(fixedDelay = 60000)
    public void batteryLogger() {
        logger.info("Saving audit logs for the current battery levels of all drones");
        batteryLogService.logBatteryLevels();
    }
}
