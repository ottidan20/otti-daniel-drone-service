package com.example.otti_daniel_drone_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.otti_daniel_drone_service.payload.response.GetAllMedicationsResponse;
import com.example.otti_daniel_drone_service.service.MedicationServiceImpl;

@RestController
@RequestMapping(path = "/api/v1/medication")
@Validated
public class MedicationController {
    @Autowired
    private MedicationServiceImpl medicationServiceImpl;

    @GetMapping(path = "/getAllMedications")
    public ResponseEntity<GetAllMedicationsResponse> getAllMedications() {
        GetAllMedicationsResponse medicationsResponse = medicationServiceImpl.getAllMedications();
        return new ResponseEntity<GetAllMedicationsResponse>(medicationsResponse, HttpStatus.OK);
    }
}
