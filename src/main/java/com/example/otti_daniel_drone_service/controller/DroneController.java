package com.example.otti_daniel_drone_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.model.EState;
import com.example.otti_daniel_drone_service.payload.request.DeliverDroneRequest;
import com.example.otti_daniel_drone_service.payload.request.LoadDroneWithMedicationRequest;
import com.example.otti_daniel_drone_service.payload.request.RegisterDroneRequest;
import com.example.otti_daniel_drone_service.payload.response.DeliverDroneResponse;
import com.example.otti_daniel_drone_service.payload.response.GetAvailableDronesResponse;
import com.example.otti_daniel_drone_service.payload.response.GetBatteryLogsResponse;
import com.example.otti_daniel_drone_service.payload.response.GetDroneBatteryLevelResponse;
import com.example.otti_daniel_drone_service.payload.response.GetLoadedDroneDetailsResponse;
import com.example.otti_daniel_drone_service.payload.response.LoadDroneWithMedicationResponse;
import com.example.otti_daniel_drone_service.payload.response.RegisterDroneResponse;
import com.example.otti_daniel_drone_service.service.BatteryLogService;
import com.example.otti_daniel_drone_service.service.DroneService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping(path = "/api/v1/drone")
@Validated
public class DroneController {

    @Autowired
    private DroneService droneService;

    @Autowired
    private BatteryLogService batteryLogService;

    /**
     * @param registerDroneRequest
     * @return Success / Error stack
     */
    @PostMapping(path = "/registerDrone")
    public ResponseEntity<RegisterDroneResponse> registerDrone(
            @Valid @NotNull @RequestBody RegisterDroneRequest registerDroneRequest) {
        RegisterDroneResponse registerDroneResponse = droneService.registerDrone(registerDroneRequest);
        return new ResponseEntity<RegisterDroneResponse>(registerDroneResponse, HttpStatus.CREATED);
    }

    /**
     * @return List of available Drones
     */
    @GetMapping(path = "/getAvailableDrones")
    public ResponseEntity<GetAvailableDronesResponse> getAvailableDrones() {
        GetAvailableDronesResponse drones = droneService.getAvailableDrones(EState.IDLE);
        return new ResponseEntity<GetAvailableDronesResponse>(drones, HttpStatus.OK);
    }

    /**
     * @return List of all Loaded Drones
     */
    @GetMapping(path = "/getLoadedDrones")
    public ResponseEntity<GetAvailableDronesResponse> getLoadedDrones() {
        GetAvailableDronesResponse drones = droneService.getAvailableDrones(EState.LOADED);
        return new ResponseEntity<GetAvailableDronesResponse>(drones, HttpStatus.OK);
    }

    /**
     * @return List of all successfully delivered drones
     */
    @GetMapping(path = "/getDeliveredDrones")
    public ResponseEntity<GetAvailableDronesResponse> getDeliveredDrones() {
        GetAvailableDronesResponse drones = droneService.getAvailableDrones(EState.DELIVERED);
        return new ResponseEntity<GetAvailableDronesResponse>(drones, HttpStatus.OK);
    }

    @GetMapping(path = "/getDroneDetails/{serialNumber}")
    public ResponseEntity<Drone> getDroneDetails(@PathVariable String serialNumber) {
        Drone drone = droneService.getDrone(serialNumber);
        return new ResponseEntity<Drone>(drone, HttpStatus.OK);
    }

    /**
     * @param serialNumber
     * @return Battery level of a drone
     */
    @GetMapping(path = "/getDroneBatteryLevel/{serialNumber}")
    public ResponseEntity<GetDroneBatteryLevelResponse> getDroneBatteryLevel(
            @Valid @NotBlank @PathVariable String serialNumber) {
        GetDroneBatteryLevelResponse getDroneBatteryLevelResponse = droneService.getBatteryLevel(serialNumber);
        return new ResponseEntity<GetDroneBatteryLevelResponse>(getDroneBatteryLevelResponse, HttpStatus.OK);
    }

    /**
     * @param loadDroneWithMedicationRequest
     * @return Success / Error stack
     */
    @PostMapping(path = "/loadDroneWithMedication")
    public ResponseEntity<LoadDroneWithMedicationResponse> loadDroneWithMedication(
            @Valid @NotNull @RequestBody LoadDroneWithMedicationRequest loadDroneWithMedicationRequest) {
        LoadDroneWithMedicationResponse loadDroneWithMedicationResponse = droneService
                .loadDroneWithMedication(loadDroneWithMedicationRequest);
        return new ResponseEntity<LoadDroneWithMedicationResponse>(loadDroneWithMedicationResponse, HttpStatus.CREATED);
    }

    /**
     * @param serialNumber
     * @return Get details of a loaded drone with medication item
     */
    @GetMapping(path = "/getLoadedDroneDetails/{serialNumber}")
    public ResponseEntity<GetLoadedDroneDetailsResponse> getLoadedDroneDetails(@PathVariable String serialNumber) {
        GetLoadedDroneDetailsResponse getLoadedDroneDetailsResponse = droneService
                .checkLoadedMedicationForDrone(serialNumber);
        return new ResponseEntity<GetLoadedDroneDetailsResponse>(getLoadedDroneDetailsResponse, HttpStatus.OK);
    }

    /**
     * @param deliverDroneRequest
     * @return Deliver medication load via drone
     */
    @PostMapping(path = "/deliverMedication")
    public ResponseEntity<DeliverDroneResponse> deliverDrone(
            @Valid @NotNull @RequestBody DeliverDroneRequest deliverDroneRequest) {
        DeliverDroneResponse deliverDroneResponse = droneService.deliverMedicalLoad(deliverDroneRequest);
        return new ResponseEntity<DeliverDroneResponse>(deliverDroneResponse, HttpStatus.CREATED);
    }

    /**
     * @return event logs of battery level
     */
    @GetMapping(path = "/getBatteryLogs")
    public ResponseEntity<GetBatteryLogsResponse> getBatteryLogs() {
        GetBatteryLogsResponse getBatteryLogsResponse = batteryLogService.getBatteryLogs();
        return new ResponseEntity<GetBatteryLogsResponse>(getBatteryLogsResponse,
                HttpStatus.OK);
    }
}
