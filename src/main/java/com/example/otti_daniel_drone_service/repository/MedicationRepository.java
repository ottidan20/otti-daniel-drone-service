package com.example.otti_daniel_drone_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.otti_daniel_drone_service.model.Medication;

import jakarta.transaction.Transactional;

@Transactional
public interface MedicationRepository extends JpaRepository<Medication, String> {
    Medication findByCode(@Param("code") String code);

    @Query(value = "select m from Medication m where m.name = :name")
    Medication findbyName(@Param("name") String name);
}
