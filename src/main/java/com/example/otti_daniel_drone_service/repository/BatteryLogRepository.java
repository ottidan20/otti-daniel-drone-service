package com.example.otti_daniel_drone_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.otti_daniel_drone_service.model.BatteryLog;

public interface BatteryLogRepository extends JpaRepository<BatteryLog, String> {

}
