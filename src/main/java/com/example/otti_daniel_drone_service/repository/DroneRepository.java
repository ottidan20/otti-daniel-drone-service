package com.example.otti_daniel_drone_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.otti_daniel_drone_service.model.Drone;
import com.example.otti_daniel_drone_service.model.EState;

import jakarta.transaction.Transactional;

@Transactional
public interface DroneRepository extends JpaRepository<Drone, String> {

    List<Drone> findAllByState(@Param("state") EState state);

    Drone findBySerialNumber(@Param("serialNumber") String serialNumber);

    @Modifying
    @Query(value = "update Drone d set d.state = :state where d.serialNumber = :serialNumber")
    void updateState(@Param("state") EState state, @Param("serialNumber") String serialNumber);
}
