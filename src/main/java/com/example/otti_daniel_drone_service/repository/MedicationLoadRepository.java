package com.example.otti_daniel_drone_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.otti_daniel_drone_service.model.MedicationLoad;

public interface MedicationLoadRepository extends JpaRepository<MedicationLoad, String> {
    @Query(value = "select m from MedicationLoad m where m.medication.code = :code")
    MedicationLoad findByCode(@Param("code") String code);

    @Query(value = "select m from MedicationLoad m where m.drone.serialNumber = :serialNumber")
    MedicationLoad findByDrone(@Param("serialNumber") String serialNumber);
}
