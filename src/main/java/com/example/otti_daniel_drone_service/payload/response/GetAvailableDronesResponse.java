package com.example.otti_daniel_drone_service.payload.response;

import java.util.List;

import com.example.otti_daniel_drone_service.model.Drone;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetAvailableDronesResponse extends BaseResponse {
    private List<Drone> drones;
}
