package com.example.otti_daniel_drone_service.payload.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class DeliverDroneResponse extends BaseResponse {
    private String serialNumber;

    private String message;
}
