package com.example.otti_daniel_drone_service.payload.response;

import com.example.otti_daniel_drone_service.model.Medication;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetLoadedDroneDetailsResponse extends BaseResponse {
    private String serialNumber;

    private Medication medication;
}
