package com.example.otti_daniel_drone_service.payload.response;

import java.time.LocalDateTime;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.EntityListeners;
import lombok.Data;

@EntityListeners(AuditingEntityListener.class)
@Data
public class CustomMessageResponse {

    private String result;

    private String message;

    private LocalDateTime timeStamp;
}
