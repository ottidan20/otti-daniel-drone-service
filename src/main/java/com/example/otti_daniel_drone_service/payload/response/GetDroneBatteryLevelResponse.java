package com.example.otti_daniel_drone_service.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GetDroneBatteryLevelResponse extends BaseResponse {

    private String serialNumber;

    private String batteryCapacity;

}
