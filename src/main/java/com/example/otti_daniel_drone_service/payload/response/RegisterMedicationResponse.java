package com.example.otti_daniel_drone_service.payload.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RegisterMedicationResponse extends BaseResponse {
    private String name;

    private String code;

    private String message;
}
