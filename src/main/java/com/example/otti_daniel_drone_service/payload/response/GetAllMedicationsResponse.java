package com.example.otti_daniel_drone_service.payload.response;

import java.util.List;

import com.example.otti_daniel_drone_service.model.Medication;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetAllMedicationsResponse extends BaseResponse {
    private List<Medication> medications;
}
