package com.example.otti_daniel_drone_service.payload.response;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class BaseResponse {
    private String result;

    private LocalDateTime timeStamp;
}
