package com.example.otti_daniel_drone_service.payload.response;

import java.util.List;

import com.example.otti_daniel_drone_service.model.BatteryLog;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GetBatteryLogsResponse extends BaseResponse {
    private List<BatteryLog> batteryLogs;
}
