package com.example.otti_daniel_drone_service.payload.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class LoadDroneWithMedicationRequest {
    @NotBlank
    private String serialNumber;

    @NotBlank
    private String source;

    @NotBlank
    private String destination;

    @NotBlank
    private String code;
}
