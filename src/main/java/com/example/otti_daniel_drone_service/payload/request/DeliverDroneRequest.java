package com.example.otti_daniel_drone_service.payload.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DeliverDroneRequest {
    @NotBlank
    private String serialNumber;
}
