package com.example.otti_daniel_drone_service.payload.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class RegisterMedicationRequest {
    @NotBlank
    private String name;

    @NotNull
    private double weight;

    @NotBlank
    private String code;

    private String imageUri;
}
