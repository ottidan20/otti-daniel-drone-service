package com.example.otti_daniel_drone_service.payload.request;

import java.math.BigDecimal;

import com.example.otti_daniel_drone_service.model.EModel;
import com.example.otti_daniel_drone_service.model.EState;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDroneRequest {
    @NotBlank
    @Size(max = 100)
    private String serialNumber;

    @NotNull
    private EModel model;

    @NotNull
    @Max(500)
    private double weightLimit;

    @NotNull(message = "Battery capacity is required")
    private BigDecimal batteryCapacity;

    @NotNull
    @Enumerated(EnumType.STRING)
    private EState state;
}
